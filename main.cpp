#include <iostream>
#include <random>>
#include <fstream>

using namespace std;
typedef vector<vector<int>> Matrix;


int getRandomValue(int start, int end) {
    random_device rd;
    mt19937 random(rd());
    uniform_int_distribution<int> dist(start, end);
    return dist(random);
}

void printMatrix(Matrix matrix) {
    for (const auto &row:matrix) {
        for (auto column : row) {
            cout << column << " ";
        }
        cout << endl;
    }
}

void saveMatrixInFile(Matrix matrix, ofstream &output) {
    for (const auto &row:matrix) {
        for (auto column : row) {
            output << column << " ";
        }
        output << endl;
    }
    output << endl;
}

Matrix getMatrix(unsigned long matrixRow, unsigned long matrixColumn) {
    return Matrix(matrixColumn, vector<int>(matrixRow, 0));
}

Matrix getRandomMatrix(unsigned long matrixRow, unsigned long matrixColumn) {
    auto matrix = Matrix(matrixColumn, vector<int>(matrixRow));
    for (auto &row : matrix) {
        for (int &column : row) {
            column = getRandomValue(1, 100);
        }
    }
    return matrix;
}

Matrix matrixDifference(Matrix left, Matrix right) {
    Matrix resultMatrix = getMatrix(left.size(), left[0].size());
    int rowOfRight = 0;
    int columnOfRight = 0;
    float columnFactor = left[0].size() / right[0].size();
    float rowFactor = left.size() / right.size();
    for (int rowOfResult = 0; rowOfResult < resultMatrix.size(); rowOfResult++) {
        for (int columnOfResult = 0; columnOfResult < resultMatrix[0].size(); columnOfResult++) {
            resultMatrix[rowOfResult][columnOfResult] =
                    left[rowOfResult][columnOfResult] - right[rowOfRight][columnOfRight];
            if ((columnOfResult + 1) % int(columnFactor) == 0) {
                columnOfRight++;
            }
        }
        if ((rowOfResult + 1) % int(rowFactor) == 0) {
            rowOfRight++;
        }
        columnOfRight = 0;
    }
    return resultMatrix;
}

int main(int *args[]) {
    auto startTime = clock();
    int d1;
    int s1;
    int d2;
    int s2;
    int outputMode;

    cout << "Enter D1" << endl;
    cin >> d1;
    cout << "Enter S1" << endl;
    cin >> s1;
    cout << "Enter D2" << endl;
    cin >> d2;
    cout << "Enter S2" << endl;
    cin >> s2;
    cout << "Selected output mode:\n"
            "1 - Print in terminal\n"
            "2 - Save in file\n"
            "3 - Print & Safe\n";
    cin >> outputMode;
    cout << endl;

    if (d1 - d2 != s1 - s2) {
        cout << "Incorrect arguments! D1-G must equal D2 and S1-G must equal S2!" << endl;
        return -1;
    }
    auto leftMatrix = getRandomMatrix(static_cast<unsigned long>(pow(2, d1)), static_cast<unsigned long>(pow(2, s1)));
    auto rightMatrix = getRandomMatrix(static_cast<unsigned long>(pow(2, d2)), static_cast<unsigned long>(pow(2, s2)));

    if (outputMode == 1 || outputMode == 3) {
        printMatrix(leftMatrix);
        cout << endl;
        printMatrix(rightMatrix);
        cout << endl;
    }

    auto resultMatrix = matrixDifference(leftMatrix, rightMatrix);

    if (outputMode == 1 || outputMode == 3) {
        printMatrix(matrixDifference(leftMatrix, rightMatrix));
        cout << endl;
    }

    if (outputMode == 2 || outputMode == 3) {
        ofstream file;
        file.open("output_" + to_string(startTime), ios::app);
        saveMatrixInFile(leftMatrix, file);
        saveMatrixInFile(rightMatrix, file);
        saveMatrixInFile(resultMatrix, file);
        file.close();

    }
    cout << "Elapsed time: " << (clock() - startTime) / CLOCKS_PER_SEC << " seconds." << endl;
    return 0;
}
